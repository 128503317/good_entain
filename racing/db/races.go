package db

import (
	"database/sql"
	"github.com/golang/protobuf/ptypes"
	_ "github.com/mattn/go-sqlite3"
	"strings"
	"sync"
	"strconv"
	"time"
	"golang.org/x/exp/slices"

	"git.neds.sh/matty/entain/racing/proto/racing"
)

// RacesRepo provides repository access to races.
type RacesRepo interface {
	// Init will initialise our races repository.
	Init() error

	// List will return a list of races.
	List(filter *racing.ListRacesRequestFilter) ([]*racing.Race, error)

	// GetRaceById will return the race indexed by ID
	GetRaceById(ID string) (*racing.Race, error)
}

type racesRepo struct {
	db   *sql.DB
	init sync.Once
}

// NewRacesRepo creates a new races repository.
func NewRacesRepo(db *sql.DB) RacesRepo {
	return &racesRepo{db: db}
}

// Init prepares the race repository dummy data.
func (r *racesRepo) Init() error {
	var err error

	r.init.Do(func() {
		// For test/example purposes, we seed the DB with some dummy races.
		err = r.seed()
	})

	return err
}

func (r *racesRepo) List(filter *racing.ListRacesRequestFilter) ([]*racing.Race, error) {
	var (
		err   error
		query string
		args  []interface{}
	)

	query = getRaceQueries()[racesList]

	query, args = r.applyFilter(query, filter)

	rows, err := r.db.Query(query, args...)
	if err != nil {
		return nil, err
	}

	return r.scanRaces(rows)
}

func (r *racesRepo) applyFilter(query string, filter *racing.ListRacesRequestFilter) (string, []interface{}) {
	var (
		clauses []string
		args    []interface{}
	)

	if filter == nil {
		return query, args
	}

	if len(filter.MeetingIds) > 0 {
		clauses = append(clauses, "meeting_id IN ("+strings.Repeat("?,", len(filter.MeetingIds)-1)+"?)")

		for _, meetingID := range filter.MeetingIds {
			args = append(args, meetingID)
		}
	}

	if len(clauses) != 0 {
		query += " WHERE " + strings.Join(clauses, " AND ")
	}

	return query, args
}

func (m *racesRepo) scanRaces(
	rows *sql.Rows,
) ([]*racing.Race, error) {
	var races []*racing.Race

	for rows.Next() {
		var race racing.Race
		var advertisedStart time.Time

		if err := rows.Scan(&race.Id, &race.MeetingId, &race.Name, &race.Number, &race.Visible, &advertisedStart); err != nil {
			if err == sql.ErrNoRows {
				return nil, nil
			}

			return nil, err
		}

		// Update status, "OPNE" for now or later on, "CLOSED" for past races.
		// Use time.Unix method as the unix timestamp has nothing to do with time zone.
		if advertisedStart.Unix() < time.Now().Unix() {
			race.Status = "CLOSED"
		} else {
			race.Status = "OPEN"
		}

		ts, err := ptypes.TimestampProto(advertisedStart)
		if err != nil {
			return nil, err
		}

		race.AdvertisedStartTime = ts

		races = append(races, &race)
	}
	// sort the whole races by advertisedStart in descending order using the slice package
	slices.SortFunc(races, func(i, j *racing.Race) bool {
		return i.AdvertisedStartTime.AsTime().After(j.AdvertisedStartTime.AsTime())
	})

	return races, nil
}

func (r *racesRepo) GetRaceById(ID string) (*racing.Race, error) {
	var (
		err   error
		query string
		args  []interface{}
	)
	IdNum := strings.Split(ID, "/")[1]
	//  IdNum should be a string contains only 0-9
	_, err = strconv.Atoi(IdNum)
	if err != nil {
		return nil, nil
	}

	query = getRaceQueries()[racesList]

	query, args = r.applyID(query, IdNum)

	rows, err := r.db.Query(query, args...)
	if err != nil {
		return nil, err
	}

	races, err := r.scanRaces(rows)
	if err != nil {
		return nil, err
	}
	if len(races) == 1 {
		return races[0], nil
	}
	return nil, nil
}

func (r *racesRepo) applyID(query string, IdNum string) (string, []interface{}) {
	var args   []interface{}

	if IdNum != "" {
		query += "WHERE id = " + IdNum
	}

	return query, args
}
