package service

import (
	"git.neds.sh/matty/entain/racing/db"
	"git.neds.sh/matty/entain/racing/proto/racing"
	"golang.org/x/net/context"
)

type Racing interface {
	// ListRaces will return a collection of races.
	ListRaces(ctx context.Context, in *racing.ListRacesRequest) (*racing.ListRacesResponse, error)
	// GetSingleRaceById will return a single race record by the ID
	GetSingleRaceById(ctx context.Context, in *racing.GetSingleRaceByIdRequest) (*racing.GetSingleRaceByIdResponse, error)
}

// racingService implements the Racing interface.
type racingService struct {
	racesRepo db.RacesRepo
}

// NewRacingService instantiates and returns a new racingService.
func NewRacingService(racesRepo db.RacesRepo) Racing {
	return &racingService{racesRepo}
}

func (s *racingService) ListRaces(ctx context.Context, in *racing.ListRacesRequest) (*racing.ListRacesResponse, error) {
	races, err := s.racesRepo.List(in.Filter)
	if err != nil {
		return nil, err
	}

	// Q1: if we have the filter of getting visible races, we can use the method: (s *racingService) GetVisibleRaces.
	if in.FilterVisible == "visible" {
		races = s.GetVisibleRaces(races)
	}

	return &racing.ListRacesResponse{Races: races}, nil
}

// The method that we can sequentially check and append the visible races.
func (s *racingService) GetVisibleRaces(races []*racing.Race) []*racing.Race {
	var visibleRaces []*racing.Race
	for _, race := range races {
		if race.Visible {
			visibleRaces = append(visibleRaces, race)
		}
	}
	return visibleRaces
}

func (s *racingService) GetSingleRaceById(ctx context.Context, in *racing.GetSingleRaceByIdRequest) (*racing.GetSingleRaceByIdResponse, error) {
	race, err := s.racesRepo.GetRaceById(in.ID)
	if err != nil {
		return nil, err
	}

	return &racing.GetSingleRaceByIdResponse{Race: race}, nil
}
