package service

import (
	"strings"
	"strconv"

	"git.neds.sh/matty/entain/sports/db"
	"git.neds.sh/matty/entain/sports/proto/sports"
	"golang.org/x/net/context"
)

type Sports interface {
	// ListEvents will return a collection of sports.
	ListEvents(ctx context.Context, in *sports.ListEventsRequest) (*sports.ListEventsResponse, error)
}

// sprotsService implements the Sports interface.
type sportsService struct {
	sportsRepo db.SportsRepo
}

// NewSportsService instantiates and returns a new sportsService.
func NewSportsService(sportsRepo db.SportsRepo) Sports {
	return &sportsService{sportsRepo}
}

func (s *sportsService) ListEvents(ctx context.Context, in *sports.ListEventsRequest) (*sports.ListEventsResponse, error) {
	Sports, err := s.sportsRepo.List(in.Filter)
	if err != nil {
		return nil, err
	}

	if in.FilterPopularity != "" {
		Sports, err= s.getSportsByPopularity(Sports, in.FilterPopularity)
		if err != nil {
			return nil, err
		}
	}

	return &sports.ListEventsResponse{Sports: Sports}, nil
}

// Get the filter of popularity and returns the list of sports
func (s *sportsService) getSportsByPopularity(Sports []*sports.Sport, filter string) ([]*sports.Sport, error){
	popularityNum := strings.Split(filter, "/")[1]
	//  num should be a int between 1 - 5
	num, err := strconv.Atoi(popularityNum)
	if err != nil || num < 1 || num > 5 {
		return nil, nil
	}

	var sportsByPopularity []*sports.Sport
	for _, sport := range Sports {
		if sport.Popularity == int64(num) {
			sportsByPopularity = append(sportsByPopularity, sport)
		}
	}
	return sportsByPopularity, nil
}
