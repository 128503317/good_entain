package db

import (
	"time"
	"log"
	"syreclabs.com/go/faker"
)

func (r *sportsRepo) seed() error {
	statement, err := r.db.Prepare(`CREATE TABLE IF NOT EXISTS sports (id INTEGER PRIMARY KEY, name TEXT, advertised_start_time DATETIME, popularity INTEGER)`)
	if err == nil {
		_, err = statement.Exec()
	}

	for i := 1; i <= 20; i++ {
		statement, err = r.db.Prepare(`INSERT OR IGNORE INTO sports(id, name, advertised_start_time, popularity) VALUES (?,?,?,?)`)
		if err == nil {
			_, err = statement.Exec(
				i,
				faker.Team().Name(),
				faker.Time().Between(time.Now().AddDate(0, 0, -1), time.Now().AddDate(0, 0, 2)).Format(time.RFC3339),
				faker.Number().Between(1, 5),
			)
		}
		if err != nil {
			log.Printf("err at %d", i)
		}

	}

	return err
}
