// The test file test the Q4 of the project. It includes fetching when id is 30, id is 101, id is not all-number and id is none.
package tests

import (
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"testing"
)

var expect_id30_file = "./q4_id30.json"
var expect_null_file = "./q4_null.json"


// the uniform function for getting response from urls
func GetUniformActual(url string) string {
	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Printf("failed at http.NewRequest")
		log.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("failed at getting response")
		log.Fatal(err)
	}
	defer resp.Body.Close()
	actualSlice, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Printf("failed at io read response")
		log.Fatal(err)
	}
	return string(actualSlice)
}

// the uniform function for getting expected results from file name
func GetUniformExpect(file *string) string {
	expectSlice, err := ioutil.ReadFile(*file)
    if err != nil {
        log.Fatal("Error when opening file: ", err)
    }
	return string(expectSlice)
}

// the uniform function for removing unexpected space of expect and actual
func RemoveSpace(expect string, actual string) (string, string) {
	expect_split := strings.Split(expect, ", ")
	expect = strings.Join(expect_split, ",")
	actual_split := strings.Split(actual, ", ")
	actual = strings.Join(actual_split, ",")

	return expect, actual
}

// test GET http://localhost:8000/v1/list-races/id/30
func TestId30Function(t *testing.T) {
	// get response when request for id:30
	var actual = GetUniformActual("http://localhost:8000/v1/list-races/id/30")

	// get expect
	var expect = GetUniformExpect(&expect_id30_file)

	// remove the spaces after "," of the string
	expect, actual = RemoveSpace(expect, actual)

	if expect != actual {
		log.Printf("The expect :\n%s\n\n", expect)
		log.Printf("The actual:\n%s\n\n", actual)
		log.Fatal("The actual is not equal to expect, the test failed.")
	}

}

// test GET http://localhost:8000/v1/list-races/id/101
func TestId101Function(t *testing.T) {
	// get response when request for id:101
	var actual = GetUniformActual("http://localhost:8000/v1/list-races/id/101")

	// get expect
	var expect = GetUniformExpect(&expect_null_file)

	// remove the spaces after "," of the string
	expect, actual = RemoveSpace(expect, actual)

	if expect != actual {
		log.Printf("The expect :\n%s\n\n", expect)
		log.Printf("The actual:\n%s\n\n", actual)
		log.Fatal("The actual is not equal to expect, the test failed.")
	}

}

// test GET http://localhost:8000/v1/list-races/id/11d
func TestIdNotAllNumFunction(t *testing.T) {
	// get response when request for id:11d
	var actual = GetUniformActual("http://localhost:8000/v1/list-races/id/11d")

	// get expect
	var expect = GetUniformExpect(&expect_null_file)

	// remove the spaces after "," of the string
	expect, actual = RemoveSpace(expect, actual)

	if expect != actual {
		log.Printf("The expect :\n%s\n\n", expect)
		log.Printf("The actual:\n%s\n\n", actual)
		log.Fatal("The actual is not equal to expect, the test failed.")
	}

}

// test GET http://localhost:8000/v1/list-races/id/
func TestIdNullFunction(t *testing.T) {
	// get response when request for id:
	var actual = GetUniformActual("http://localhost:8000/v1/list-races/id/")

	// get expect
	var expect = GetUniformExpect(&expect_null_file)

	// remove the spaces after "," of the string
	expect, actual = RemoveSpace(expect, actual)

	if expect != actual {
		log.Printf("The expect :\n%s\n\n", expect)
		log.Printf("The actual:\n%s\n\n", actual)
		log.Fatal("The actual is not equal to expect, the test failed.")
	}

}
