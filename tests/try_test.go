package tests

import (
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"testing"
)

var expect_file = "./try_test.json"

func TestBasicFunction(t *testing.T) {
	// get response
	client := &http.Client{}
	var data = strings.NewReader(`{
  		"filter": {}
	}`)
	req, err := http.NewRequest("POST", "http://localhost:8000/v1/list-races", data)
	if err != nil {
		log.Printf("failed at http.NewRequest")
		log.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("failed at getting response")
		log.Fatal(err)
	}
	defer resp.Body.Close()
	actualSlice, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Printf("failed at io read response")
		log.Fatal(err)
	}
	var actual = string(actualSlice)

	// get expect
	expectSlice, err := ioutil.ReadFile(expect_file)
    if err != nil {
        log.Fatal("Error when opening file: ", err)
    }
	var expect = string(expectSlice)

	// remove the spaces after "," of the string
	expect_split := strings.Split(expect, ", ")
	expect = strings.Join(expect_split, ",")
	actual_split := strings.Split(actual, ", ")
	actual = strings.Join(actual_split, ",")

	if expect != actual {
		log.Printf("The expect ReplaceAll :\n%s\n\n", expect)
		log.Printf("The actual ReplaceAll:\n%s\n\n", actual)
		log.Fatal("The actual is not equal to expect, the test failed.")
	}

}
