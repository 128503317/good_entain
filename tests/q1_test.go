// The test file test the Q1 of the project, which includes getting the visible races and continue to be able to fetch all races afterwards.
package tests

import (
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"testing"
)

var expect_file_visible = "./q1_test.json"
var expect_file_original = "./try_test.json"

func TestVisibleFunction(t *testing.T) {
	// get visible response.
	client := &http.Client{}
	req, err := http.NewRequest("GET", "http://localhost:8000/v1/visible/list-races", nil)
	if err != nil {
		log.Printf("failed at http.NewRequest")
		log.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("failed at getting response")
		log.Fatal(err)
	}
	defer resp.Body.Close()
	actualSlice, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Printf("failed at io read response")
		log.Fatal(err)
	}
	var actual = string(actualSlice)

	// get visible expect.
	expectSlice, err := ioutil.ReadFile(expect_file_visible)
    if err != nil {
        log.Fatal("Error when opening file: ", err)
    }
	var expect = string(expectSlice)

	// remove the spaces after "," of the string
	expect_split := strings.Split(expect, ", ")
	expect = strings.Join(expect_split, ",")
	actual_split := strings.Split(actual, ", ")
	actual = strings.Join(actual_split, ",")

	// test the visible
	if expect != actual {
		log.Printf("The expect :\n%s\n\n", expect)
		log.Printf("The actual :\n%s\n\n", actual)
		log.Fatal("The visible actual is not equal to visible expect, the test failed.")
	}

	// After fetching the visible races, we can continue to get all the races.
	// get all the resonse.
	req2, err := http.NewRequest("GET", "http://localhost:8000/v1/list-races", nil)
	if err != nil {
		log.Printf("failed at http.NewRequest ep2")
		log.Fatal(err)
	}
	req2.Header.Set("Content-Type", "application/json")
	resp2, err := client.Do(req2)
	if err != nil {
		log.Printf("failed at getting response ep2")
		log.Fatal(err)
	}
	defer resp2.Body.Close()
	actualSlice2, err := io.ReadAll(resp2.Body)
	if err != nil {
		log.Printf("failed at io read response")
		log.Fatal(err)
	}
	actual = string(actualSlice2)

	// get all the expect.
	expectSlice2, err := ioutil.ReadFile(expect_file_original)
    if err != nil {
        log.Fatal("Error when opening file: ", err)
    }
	expect = string(expectSlice2)

	// remove the spaces after "," of the string
	expect_split = strings.Split(expect, ", ")
	expect = strings.Join(expect_split, ",")
	actual_split = strings.Split(actual, ", ")
	actual = strings.Join(actual_split, ",")

	// test the visible.
	if expect != actual {
		log.Printf("The expect :\n%s\n\n", expect)
		log.Printf("The expect length:\n%d\n\n", len(expect))
		log.Printf("The actual :\n%s\n\n", actual)
		log.Printf("The actual length:\n%d\n\n", len(actual))
		log.Fatal("The actual is not equal to expect, the test failed.")
	}

}
