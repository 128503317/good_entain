// The test file test the Q5 of the project, which includes basic test of fetching all record of sports and filter tests of popularity.
package tests

import (
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"testing"
)

var expect_file_basic = "./q5_basic_test.json"
var expect_file_popularity5 = "./q5_popularity5.json"
var expect_file_popularity1 = "./q5_popularity1.json"
var expect_file_null = "./q5_popularity_null.json"

// the uniform function for getting response from urls
func GetUniformActualEvents(url string) string {
	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Printf("failed at http.NewRequest")
		log.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("failed at getting response")
		log.Fatal(err)
	}
	defer resp.Body.Close()
	actualSlice, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Printf("failed at io read response")
		log.Fatal(err)
	}
	return string(actualSlice)
}

// the uniform function for getting expected results from file name
func GetUniformExpectEvents(file *string) string {
	expectSlice, err := ioutil.ReadFile(*file)
    if err != nil {
        log.Fatal("Error when opening file: ", err)
    }
	return string(expectSlice)
}

// the uniform function for removing unexpected space of expect and actual
func RemoveSpaceEvents(expect string, actual string) (string, string) {
	expect_split := strings.Split(expect, ", ")
	expect = strings.Join(expect_split, ",")
	actual_split := strings.Split(actual, ", ")
	actual = strings.Join(actual_split, ",")

	return expect, actual
}

// test basic POST request to get all the records of ListEvents
func TestBasicSportsFunction(t *testing.T) {
	// get response
	client := &http.Client{}
	var data = strings.NewReader(`{
  		"filter": {}
	}`)
	req, err := http.NewRequest("POST", "http://localhost:8000/v1/list-events", data)
	if err != nil {
		log.Printf("failed at http.NewRequest")
		log.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("failed at getting response")
		log.Fatal(err)
	}
	defer resp.Body.Close()
	actualSlice, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Printf("failed at io read response")
		log.Fatal(err)
	}
	var actual = string(actualSlice)

	// get expect
	var expect = GetUniformExpectEvents(&expect_file_basic)

	// remove the spaces after "," of the string
	expect, actual = RemoveSpaceEvents(expect, actual)

	if expect != actual {
		log.Printf("The expect:\n%s\n\n", expect)
		log.Printf("The actual:\n%s\n\n", actual)
		log.Fatal("The actual is not equal to expect, the test failed.")
	}

}

// test GET request to list the popularity/5 of ListEvents
func TestFilterPopularity5(t *testing.T) {
	// get response.
	var actual = GetUniformActualEvents("http://localhost:8000/v1/list-events/popularity/5")

	// get expect.
	var expect = GetUniformExpectEvents(&expect_file_popularity5)

	// remove the spaces after "," of the string
	expect, actual = RemoveSpaceEvents(expect, actual)

	// test the filter
	if expect != actual {
		log.Printf("The expect :\n%s\n\n", expect)
		log.Printf("The actual :\n%s\n\n", actual)
		log.Fatal("The actual is not equal to expect, the test failed.")
	}

	// After fetching the sports of filter, we can continue to get all the sports.
	// get all the resonse.
	actual = GetUniformActualEvents("http://localhost:8000/v1/list-events")

	// get all the expect.
	expect = GetUniformExpectEvents(&expect_file_basic)

	// remove the spaces after "," of the string
	expect, actual = RemoveSpaceEvents(expect, actual)

	// test the visible.
	if expect != actual {
		log.Printf("The expect :\n%s\n\n", expect)
		log.Printf("The actual :\n%s\n\n", actual)
		log.Fatal("The actual is not equal to expect, the test failed.")
	}

}

// test GET request to list the popularity/1 of ListEvents
func TestFilterPopularity1(t *testing.T) {
	// get response.
	var actual = GetUniformActualEvents("http://localhost:8000/v1/list-events/popularity/1")

	// get expect.
	var expect = GetUniformExpectEvents(&expect_file_popularity1)

	// remove the spaces after "," of the string
	expect, actual = RemoveSpaceEvents(expect, actual)

	// test the filter
	if expect != actual {
		log.Printf("The expect :\n%s\n\n", expect)
		log.Printf("The actual :\n%s\n\n", actual)
		log.Fatal("The actual is not equal to expect, the test failed.")
	}

	// After fetching the sports of filter, we can continue to get all the sports.
	// get all the resonse.
	actual = GetUniformActualEvents("http://localhost:8000/v1/list-events")

	// get all the expect.
	expect = GetUniformExpectEvents(&expect_file_basic)

	// remove the spaces after "," of the string
	expect, actual = RemoveSpaceEvents(expect, actual)

	// test the visible.
	if expect != actual {
		log.Printf("The expect :\n%s\n\n", expect)
		log.Printf("The actual :\n%s\n\n", actual)
		log.Fatal("The actual is not equal to expect, the test failed.")
	}
}

// test GET request to list the popularity/6 of ListEvents, which should be no record
func TestFilterPopularity6(t *testing.T) {
	// get response.
	var actual = GetUniformActualEvents("http://localhost:8000/v1/list-events/popularity/6")

	// get expect.
	var expect = GetUniformExpectEvents(&expect_file_null)

	// remove the spaces after "," of the string
	expect, actual = RemoveSpaceEvents(expect, actual)

	// test the filter
	if expect != actual {
		log.Printf("The expect :\n%s\n\n", expect)
		log.Printf("The actual :\n%s\n\n", actual)
		log.Fatal("The actual is not equal to expect, the test failed.")
	}
}

// test GET request to list the popularity/23df of ListEvents, which should be no record
func TestFilterPopularityNotAllNum(t *testing.T) {
	// get response.
	var actual = GetUniformActualEvents("http://localhost:8000/v1/list-events/popularity/23df")

	// get expect.
	var expect = GetUniformExpectEvents(&expect_file_null)

	// remove the spaces after "," of the string
	expect, actual = RemoveSpaceEvents(expect, actual)

	// test the filter
	if expect != actual {
		log.Printf("The expect :\n%s\n\n", expect)
		log.Printf("The actual :\n%s\n\n", actual)
		log.Fatal("The actual is not equal to expect, the test failed.")
	}
}

// test GET request to list the popularity/ of ListEvents, which should be no record
func TestFilterPopularityNull(t *testing.T) {
	// get response.
	var actual = GetUniformActualEvents("http://localhost:8000/v1/list-events/popularity/")

	// get expect.
	var expect = GetUniformExpectEvents(&expect_file_null)

	// remove the spaces after "," of the string
	expect, actual = RemoveSpaceEvents(expect, actual)

	// test the filter
	if expect != actual {
		log.Printf("The expect :\n%s\n\n", expect)
		log.Printf("The actual :\n%s\n\n", actual)
		log.Fatal("The actual is not equal to expect, the test failed.")
	}
}
